import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', redirectTo: 'demo', pathMatch: 'full' },
      {
        path: 'demo',
        loadChildren: () =>
          import('./demo/demo.module').then((t) => t.DemoModule),
      },
    ]),
  ],
})
export class TrainingModule {}
