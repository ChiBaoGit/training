import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { StringChain } from 'lodash';

@Component({
  selector: 'ga-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss'],
})
export class Demo2Component implements OnInit {
  a = {
    name: '',
    age: 0,
  };
  @Input() name = 'abc';
  @Output() valueChange = new EventEmitter();
  constructor() {}

  ngOnInit() {}
  checkValid() {
    if (this.a.name && this.a.age) {
      this.valueChange.emit(this.a);
    }
  }
}
