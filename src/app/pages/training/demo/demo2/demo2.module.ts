import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Demo2Component } from './demo2.component';
import { NbInputModule } from '@nebular/theme';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [Demo2Component],
  imports: [CommonModule, NbInputModule, FormsModule],
  exports: [Demo2Component],
})
export class Demo2Module {}
