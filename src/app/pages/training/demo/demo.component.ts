import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ga-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss'],
})
export class DemoComponent implements OnInit {
  b = {};
  constructor() {}

  ngOnInit(): void {}

  a(e) {
    console.log(e);
  }
}
