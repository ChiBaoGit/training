import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoComponent } from './demo.component';
import { RouterModule } from '@angular/router';
import { Demo2Module } from './demo2/demo2.module';

@NgModule({
  declarations: [DemoComponent],
  imports: [
    CommonModule,
    Demo2Module,
    RouterModule.forChild([{ path: '', component: DemoComponent }]),
  ],
})
export class DemoModule {}
