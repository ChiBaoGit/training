import { NbMenuItem } from '@nebular/theme';
import { AppIdEnum } from '../shared/services/phan-quyen/chuc-nang.interface';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Công trình xây dựng',
    link: '/cong-trinh-xay-dung/cong-trinh',
    data: AppIdEnum['CongTrinhXayDung'],
    hidden: true,
  },
  { title: 'Khác', group: true },
  {
    title: 'Training',
    link: '/training/demo',
  },
];
